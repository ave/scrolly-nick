from st3m.application import Application, ApplicationContext
from st3m.goose import Tuple, Any
from st3m.input import InputState
from st3m.goose import Optional
from st3m.ui.view import ViewManager
from ctx import Context
import leds
import json
import math

CONFIG_SCHEMA: dict[str, dict[str, Any]] = {
    "name": {"types": [str]},
    "name_font": {"types": [int, float], "cast_to": int},
    "size": {"types": [int, float], "cast_to": int},
    "pronouns": {"types": ["list_of_str"]},
    "pronouns_size": {"types": [int, float], "cast_to": int},
    "pronouns_font": {"types": [int, float], "cast_to": int},
    "pronouns_offset": {"types": [int, float]},
    "text_scroll_divisor": {"types": [int, float]},
    "color": {"types": ["hex_color"]},
}


class Configuration:
    def __init__(self) -> None:
        self.name = "no scrolly_nick.json"
        self.name_font: int = 8
        self.size: int = 150
        self.pronouns: list[str] = []
        self.pronouns_size: int = 30
        self.pronouns_font: int = 6
        self.pronouns_offset: int = -75
        self.text_scroll_divisor: int = 5
        self.color = "0xca9bf7"

        self.config_errors: list[str] = []

    @classmethod
    def load(cls, path: str) -> "Configuration":
        res = cls()
        try:
            with open(path) as f:
                jsondata = f.read()
            data = json.loads(jsondata)
        except OSError:
            data = {}
        except ValueError:
            res.config_errors = ["scrolly_nick.json", "decode failed!"]
            data = {}

        # verify the config format and generate an error message
        config_type_errors: list[str] = []
        for config_key, type_data in CONFIG_SCHEMA.items():
            if config_key not in data.keys():
                continue
            key_type_valid = False
            for allowed_type in type_data["types"]:
                if isinstance(allowed_type, type):
                    if isinstance(data[config_key], allowed_type):
                        key_type_valid = True
                        break
                elif allowed_type == "list_of_str":
                    if isinstance(data[config_key], list) and (
                        len(data[config_key]) == 0
                        or set([type(x) for x in data[config_key]]) == {str}
                    ):
                        key_type_valid = True
                        break
                elif allowed_type == "hex_color":
                    if (
                        isinstance(data[config_key], str)
                        and data[config_key][0:2] == "0x"
                        and len(data[config_key]) == 8
                    ):
                        key_type_valid = True
                        break

            if not key_type_valid:
                config_type_errors.append(config_key)
            else:
                # Cast to relevant type if needed
                if type_data.get("cast_to"):
                    data[config_key] = type_data["cast_to"](data[config_key])
                setattr(res, config_key, data[config_key])

        if config_type_errors:
            res.config_errors += [
                "data types wrong",
                "in scrolly_nick.json",
                "for:",
            ] + config_type_errors

        return res

    def save(self, path: str) -> None:
        d = {
            config_key: getattr(self, config_key) for config_key in CONFIG_SCHEMA.keys()
        }

        jsondata = json.dumps(d)
        with open(path, "w") as f:
            f.write(jsondata)
            f.close()

    def to_normalized_tuple(self) -> Tuple[float, float, float]:
        return (
            int(self.color[2:4], 16) / 255.0,
            int(self.color[4:6], 16) / 255.0,
            int(self.color[6:8], 16) / 255.0,
        )


class NickApp(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self._text_x = 0.0
        self._filename = "/flash/scrolly_nick.json"
        self._config = Configuration.load(self._filename)
        self._pronouns_serialized = " ".join(self._config.pronouns)
        self._rendered_text_size = 0

        self._time = 0
        self._led_rotation = 0

    def on_enter(self, vm: Optional[ViewManager]) -> None:
        self._text_x = 0.0
        self._time = 0
        self._led_rotation = 0
        leds.set_brightness(69)
        leds.update()

    def draw(self, ctx: Context) -> None:
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font_size = self._config.size
        ctx.font = ctx.get_font_name(self._config.name_font)

        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        ctx.rgb(*self._config.to_normalized_tuple())

        if self._config.config_errors:
            text_to_draw = [
                "data types wrong",
                "in nick.json for:",
                "",
            ] + self._config.config_errors
            draw_y = (-20 * len(text_to_draw)) / 2
            ctx.move_to(0, draw_y)
            ctx.font_size = 20
            # 0xff4500, red
            ctx.rgb(1, 0.41, 0)
            ctx.font = ctx.get_font_name(8)
            for config_error in text_to_draw:
                draw_y += 20
                ctx.move_to(0, draw_y)
                ctx.text(config_error)
            return

        if not self._rendered_text_size:
            self._rendered_text_size = ctx.text_width(self._config.name + " ")
            return

        ctx.move_to(self._text_x, 0)
        # draw enough of the name to fit in the screen, then some more
        ctx.text((self._config.name + " ") * (math.ceil(240 / self._rendered_text_size) + 2))

        if self._pronouns_serialized:
            ctx.font = ctx.get_font_name(self._config.pronouns_font)
            ctx.move_to(0, self._config.pronouns_offset)
            ctx.font_size = self._config.pronouns_size
            ctx.text(self._pronouns_serialized)

        # from https://git.flow3r.garden/rahix/nick-rahix/-/blob/main/__init__.py
        color1 = 274
        color2 = 329
        for i in range(21):
            i20 = i / 20
            hue = i20 * color2 + (1 - i20) * color1
            value = i20 * 0.5 + (1 - i20) * 0.2
            leds.set_hsv((i + self._led_rotation) % 20, hue, 1, value)
            leds.set_hsv(39 - (i + self._led_rotation) % 20, hue, 1, value)
        leds.update()

    def on_exit(self) -> None:
        leds.set_all_hsv(0, 0, 0)
        leds.update()
        if not self._config.config_errors:
            self._config.save(self._filename)

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)

        self._time += delta_ms / 1000
        self._led_rotation = math.floor(self._time * 5) % 40

        move_speed = self._rendered_text_size / self._config.text_scroll_divisor

        self._text_x -= move_speed * (delta_ms / 1000)
        if self._text_x <= -self._rendered_text_size:
            self._text_x += self._rendered_text_size


# For running with `mpremote run`:
if __name__ == "__main__":
    import st3m.run

    st3m.run.run_view(NickApp(ApplicationContext()))
